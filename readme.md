These wallpapers have lot of sources:

* https://wallhaven.cc
* https://www.hdwallpapers.in
* https://wallpaperswide.com
* https://www.pexels.com
* Unknown

If you find an image of this repository with limited use, let me know and I will
remove it.
